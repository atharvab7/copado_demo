import { LightningElement, wire, track } from 'lwc';

// Import the URL for the static resources
import double_opt_in from '@salesforce/resourceUrl/double_opt_in';
import inquiry_qualification from '@salesforce/resourceUrl/inquiry_qualification';
import nurture_campaign from '@salesforce/resourceUrl/nurture_campaign';
import document_collection from '@salesforce/resourceUrl/document_collection';
import support_handling from '@salesforce/resourceUrl/support_handling';
import resourceUrl from '@salesforce/resourceUrl/test';

// New Design Images
import Lend_expertise from '@salesforce/resourceUrl/Lend_expertise';
import Run_bulk_promotions from '@salesforce/resourceUrl/Run_bulk_promotions';
import Increase_show_rates from '@salesforce/resourceUrl/Increase_show_rates';
import Qualify_inquiries from '@salesforce/resourceUrl/Qualify_inquiries';
import Nurture_subscribers from '@salesforce/resourceUrl/Nurture_subscribers';
import Respond_the_go from '@salesforce/resourceUrl/Respond_the_go';
import Auto_reply_when from '@salesforce/resourceUrl/Auto_reply_when';
import Avoid_phone_tags from '@salesforce/resourceUrl/Avoid_phone_tags';
import Collect_documents from '@salesforce/resourceUrl/Collect_documents';
import Share_status_updates from '@salesforce/resourceUrl/Share_status_updates';
import Capture_optins from '@salesforce/resourceUrl/Capture_optins';
import Payment_reminders from '@salesforce/resourceUrl/Payment_reminders';
import Attend_service_requests from '@salesforce/resourceUrl/Attend_service_requests';
import Get_survey_responses from '@salesforce/resourceUrl/Get_survey_responses';
import Grow_digram from '@salesforce/resourceUrl/Grow_digram';




import refreshApex from '@salesforce/apex';
import getObjectPrefix from '@salesforce/apex/TrialOrgController.getObjectPrefix';
import getCreatedLeadId from '@salesforce/apex/TrialOrgController.getCreatedLeadId';
import triggerInquiryMessage from '@salesforce/apex/TrialOrgController.triggerInquiryMessage';
import triggerNPSSurveyMessage from '@salesforce/apex/TrialOrgController.triggerNPSSurveyMessage';
import getCurrentUserNameDataOfApp from '@salesforce/apex/TrialOrgController.getCurrentUserName';
import getToolTipStatus from '@salesforce/apex/TrialOrgController.getToolTipManagerStatus';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import FIRST_NAME from '@salesforce/schema/Lead.FirstName';
import LAST_NAME from '@salesforce/schema/Lead.LastName';
import MOBILE_PHONE from '@salesforce/schema/Lead.MobilePhone';
import NAME from '@salesforce/schema/Lead.Name';
import LEAD_SOURCE from '@salesforce/schema/Lead.LeadSource';


// Import custom labels
import Conversational_Info from '@salesforce/label/c.Conversational_Info';
import Conversational_Messaging from '@salesforce/label/c.Conversational_Messaging';
import Conversational_Avoid from '@salesforce/label/c.Conversational_Avoid';
import Conversational_Ready from '@salesforce/label/c.Conversational_Ready';
import Double_opt_in from '@salesforce/label/c.Double_opt_in';
import Include_Request from '@salesforce/label/c.Include_Request';
import Explore_Double_opt from '@salesforce/label/c.Explore_Double_opt';
import The_Inquiry_Qualification from '@salesforce/label/c.The_Inquiry_Qualification';
import Includes_two_sample from '@salesforce/label/c.Includes_two_sample';
import Trigger_Flow from '@salesforce/label/c.Trigger_Flow';
import The_Nurture_Campaign from '@salesforce/label/c.The_Nurture_Campaign';
import Includes_two_campaign from '@salesforce/label/c.Includes_two_campaign';
import Trigger_Campaign from '@salesforce/label/c.Trigger_Campaign';
import The_Document_Collection from '@salesforce/label/c.The_Document_Collection';
import Includes_document_request from '@salesforce/label/c.Includes_document_request';
import Experience_Flow from '@salesforce/label/c.Experience_Flow';
import ExperienceSupport_survey from '@salesforce/label/c.ExperienceSupport_survey';
import Includes_initiation_handling from '@salesforce/label/c.Includes_initiation_handling';
import Case_studies from '@salesforce/label/c.Case_studies';
import Read_Story from '@salesforce/label/c.Read_Story';

// New Trial home Labels start here
import trial_Conversion from '@salesforce/label/c.trial_Conversion';
import trial_Lend_expertise from '@salesforce/label/c.trial_Lend_expertise';
import trial_Lend_discription from '@salesforce/label/c.trial_Lend_discription';
import trial_Run_bulk from '@salesforce/label/c.trial_Run_bulk';
import trial_Run_bulk_discription from '@salesforce/label/c.trial_Run_bulk_discription';
import trial_Boost_appointment from '@salesforce/label/c.trial_Boost_appointment';
import trial_Boost_appointment_discription from '@salesforce/label/c.trial_Boost_appointment_discription';
import trial_Qualify_inquiries from '@salesforce/label/c.trial_Qualify_inquiries';
import trial_Qualify_inquiries_discription from '@salesforce/label/c.trial_Qualify_inquiries_discription';
import trial_Nurture_interest from '@salesforce/label/c.trial_Nurture_interest';
import trial_Nurture_interest_discription from '@salesforce/label/c.trial_Nurture_interest_discription';
import trial_Productivity from '@salesforce/label/c.trial_Productivity';
import trial_Respond from '@salesforce/label/c.trial_Respond';
import trial_Respond_discription from '@salesforce/label/c.trial_Respond_discription';
import trial_Auto_reply from '@salesforce/label/c.trial_Auto_reply';
import trial_Auto_reply_discription from '@salesforce/label/c.trial_Auto_reply_discription';
import trial_Avoid_phone from '@salesforce/label/c.trial_Avoid_phone';
import trial_Avoid_phone_discription from '@salesforce/label/c.trial_Avoid_phone_discription';
import trial_Onboarding from '@salesforce/label/c.trial_Onboarding';
import trial_Speed_up from '@salesforce/label/c.trial_Speed_up';
import trial_Speed_up_discription from '@salesforce/label/c.trial_Speed_up_discription';
import trial_Share_status from '@salesforce/label/c.trial_Share_status';
import trial_Share_status_discription from '@salesforce/label/c.trial_Share_status_discription';
import trial_Compliance from '@salesforce/label/c.trial_Compliance';
import trial_Grow_list from '@salesforce/label/c.trial_Grow_list';
import trial_Grow_list_discription from '@salesforce/label/c.trial_Grow_list_discription';
import trial_Cash_flow from '@salesforce/label/c.trial_Cash_flow';
import trial_Send_payment from '@salesforce/label/c.trial_Send_payment';
import trial_Send_payment_discription from '@salesforce/label/c.trial_Send_payment_discription';
import trial_Retention from '@salesforce/label/c.trial_Retention';
import trial_Attend_service from '@salesforce/label/c.trial_Attend_service';
import trial_Attend_service_discription from '@salesforce/label/c.trial_Attend_service_discription';
import trial_Advocacy from '@salesforce/label/c.trial_Advocacy';
import trial_Get_survey from '@salesforce/label/c.trial_Get_survey';
import trial_Get_survey_discription from '@salesforce/label/c.trial_Get_survey_discription';
import try_now from '@salesforce/label/c.try_now';
import try_paid_version from '@salesforce/label/c.try_paid_version';
import Upgrade_Unlock from '@salesforce/label/c.Upgrade_Unlock';
import Book_Demo from '@salesforce/label/c.Book_Demo';

export default class TrialOrgMainComponent extends LightningElement {

    // Expose the static resource URL for use in the template

    @track userName;
    @track isViewMoreClicked = false;
    @track welcomeMsg;

    double_opt_in = double_opt_in;
    inquiry_qualification = inquiry_qualification;
    nurture_campaign = nurture_campaign;
    document_collection = document_collection;
    support_handling = support_handling;
    resourceUrl = resourceUrl;
    selectedCase;


    // New Design Images
    Lend_expertise = Lend_expertise;
    Run_bulk_promotions = Run_bulk_promotions;
    Increase_show_rates = Increase_show_rates;
    Qualify_inquiries = Qualify_inquiries;
    Nurture_subscribers = Nurture_subscribers;
    Respond_the_go = Respond_the_go;
    Auto_reply_when = Auto_reply_when;
    Avoid_phone_tags = Avoid_phone_tags;
    Collect_documents = Collect_documents;
    Share_status_updates = Share_status_updates;
    Capture_optins = Capture_optins;
    Payment_reminders = Payment_reminders;
    Attend_service_requests = Attend_service_requests;
    Get_survey_responses = Get_survey_responses;
    Grow_digram = Grow_digram;

    caseImageMap = {
        'Lend expertise' : this.Grow_digram,
        'Nurture subscribers' : this.Nurture_subscribers,
    }

    isLightning;
    consentObjectPrefix;
    objectForPrefixName = 'Lead';
    isModalOpen = false;
    toastMessage = '';
    viewButtonText = 'View More'


    fields = [FIRST_NAME, LAST_NAME, MOBILE_PHONE, NAME, LEAD_SOURCE];
    // Expose the labels to use in the template.
    label = {
        Conversational_Messaging,
        Conversational_Info,
        Conversational_Avoid,
        Conversational_Ready,
        Double_opt_in,
        Include_Request,
        Explore_Double_opt,
        The_Inquiry_Qualification,
        Includes_two_sample,
        Trigger_Flow,
        The_Nurture_Campaign,
        Includes_two_campaign,
        Trigger_Campaign,
        The_Document_Collection,
        Includes_document_request,
        Experience_Flow,
        ExperienceSupport_survey,
        Includes_initiation_handling,
        Case_studies,
        Read_Story,

        // New trial label expose
        trial_Conversion,
        trial_Lend_expertise,
        trial_Lend_discription,
        trial_Run_bulk,
        trial_Run_bulk_discription,
        trial_Boost_appointment,
        trial_Boost_appointment_discription,
        trial_Qualify_inquiries,
        trial_Qualify_inquiries_discription,
        trial_Nurture_interest,
        trial_Nurture_interest_discription,
        trial_Productivity,
        trial_Respond,
        trial_Respond_discription,
        trial_Auto_reply,
        trial_Auto_reply_discription,
        trial_Avoid_phone,
        trial_Avoid_phone_discription,
        trial_Onboarding,
        trial_Speed_up,
        trial_Speed_up_discription,
        trial_Share_status,
        trial_Share_status_discription,
        trial_Compliance,
        trial_Grow_list,
        trial_Grow_list_discription,
        trial_Cash_flow,
        trial_Send_payment,
        trial_Send_payment_discription,
        trial_Retention,
        trial_Attend_service,
        trial_Attend_service_discription,
        trial_Advocacy,
        trial_Get_survey,
        trial_Get_survey_discription,
        try_now,
        try_paid_version,
        Upgrade_Unlock,
        Book_Demo
    };

    caseStudyInfoWithOutCategory = [];

    caseStudyInfoWithCategory = [

        {
            name: this.label.trial_Conversion,
            value: [
                {
                    label: this.label.trial_Lend_expertise,
                    caseName: this.label.trial_Conversion,
                    image: this.Lend_expertise,
                    description: this.label.trial_Lend_discription,
                    isActive: true,
                    popUpdata: ''

                },
                {
                    label: this.label.trial_Run_bulk,
                    caseName: this.label.trial_Conversion,
                    image: this.Run_bulk_promotions,
                    description: this.label.trial_Run_bulk_discription,
                    isActive: true,
                    popUpdata: ''
                },
                {
                    label: this.label.trial_Boost_appointment,
                    caseName: this.label.trial_Conversion,
                    image: this.Increase_show_rates,
                    description: this.label.trial_Boost_appointment_discription,
                    isActive: true,
                    popUpdata: ''
                },
                {
                    label: this.label.trial_Qualify_inquiries,
                    caseName: this.label.trial_Conversion,
                    image: this.Qualify_inquiries,
                    description: this.label.trial_Qualify_inquiries_discription,
                    isActive: false,
                    popUpdata: ''
                },
                {
                    label: this.label.trial_Nurture_interest,
                    caseName: this.label.trial_Conversion,
                    image: this.Nurture_subscribers,
                    description: this.label.trial_Nurture_interest_discription,
                    isActive: true,
                    popUpdata: ''
                }
            ]
        },
        {
            name: this.label.trial_Productivity,
            value: [
                {
                    label: this.label.trial_Respond,
                    caseName: this.label.trial_Productivity,
                    image: this.Respond_the_go,
                    description: this.label.trial_Respond_discription,
                    isActive: false,
                    popUpdata: ''
                },
                {
                    label: this.label.trial_Auto_reply,
                    caseName: this.label.trial_Productivity,
                    image: this.Auto_reply_when,
                    description: this.label.trial_Auto_reply_discription,
                    isActive: false,
                    popUpdata: ''
                },
                {
                    label: this.label.trial_Avoid_phone,
                    caseName: this.label.trial_Productivity,
                    image: this.Avoid_phone_tags,
                    description: this.label.trial_Avoid_phone_discription,
                    isActive: false,
                    popUpdata: ''
                }

            ]
        },
        {
            name: this.label.trial_Onboarding,
            value: [
                {
                    label: this.label.trial_Speed_up,
                    caseName: this.label.trial_Onboarding,
                    image: this.Collect_documents,
                    description: this.label.trial_Speed_up_discription,
                    isActive: false,
                    popUpdata: ''
                },
                {
                    label: this.label.trial_Share_status,
                    caseName: this.label.trial_Onboarding,
                    image: this.Share_status_updates,
                    description: this.label.trial_Share_status_discription,
                    isActive: false,
                    popUpdata: ''
                }
            ]
        },
        {
            name: this.label.trial_Compliance, 
            value: [
                {
                    label: this.label.trial_Grow_list, 
                    caseName: this.label.trial_Compliance,
                    image: this.Capture_optins,
                    description: this.label.trial_Grow_list_discription, 
                    isActive: true,
                    popUpdata: ''
                }
            ]
        },
        {
            name: this.label.trial_Cash_flow, 
            value: [
                {
                    label: this.label.trial_Send_payment, 
                    caseName: this.label.trial_Cash_flow,
                    image: this.Payment_reminders,
                    description: this.label.trial_Send_payment_discription, 
                    isActive: false,
                    popUpdata: ''
                }
            ]
        },
        {
            name: this.label.trial_Retention, 
            value: [
                {
                    label: this.label.trial_Attend_service, 
                    caseName: this.label.trial_Retention,
                    image: this.Attend_service_requests,
                    description: this.label.trial_Attend_service_discription, 
                    isActive: false,
                    popUpdata: ''
                }
            ]
        },
        {
            name: this.label.trial_Advocacy,  
            value: [
                {
                    label: this.label.trial_Get_survey, 
                    caseName: this.label.trial_Advocacy,
                    image: this.Get_survey_responses,
                    description: this.label.trial_Get_survey_discription,  
                    isActive: false,
                    popUpdata: ''
                }
            ]
        }

    ];

    connectedCallback() {
        this.isLightningExperienceOrSalesforce1();
        getCurrentUserNameDataOfApp().then((res) => {
            this.userName = res;
            this.welcomeMsg = 'Hi ' + this.userName + ', welcome to your free trial!';
        });

        //get data from 

        this.caseStudyInfoWithCategory.forEach((categories) => {
            if (categories.value && Array.isArray(categories.value)) {
                categories.value.forEach((cases) => {
                    this.caseStudyInfoWithOutCategory.push(cases);
                })
            }
        });

        getToolTipStatus({ eventName: 'doubleOptIn' }).then((res) => {
            if (res) {
                let toolTipdata = {
                    headerMessage: 'WELCOME TO THE SMS-MAGIC TRIAL',
                    infoMessage: 'To experience conversational messaging, start by creating a lead with your own name and mobile phone number.',
                    firstButtonText: 'Got It',
                    secondButtonText: 'Create Lead',
                    firstActionFunction: function () {
                        this.redirectToLeadObject();
                    }.bind(this),
                    secondActionFunction: function () {
                        this.redirectToLeadObject();
                    }.bind(this)
                }
                let isToolTipOpen = this.openToolTip(toolTipdata);
                if (isToolTipOpen) {
                    console.log(isToolTipOpen);
                }
            }
        }).catch((error) => {
            console.log(error);
        })
    }


    isLightningExperienceOrSalesforce1() {
        if (document.referrer.indexOf(".lightning.force.com") > 0) {
            this.isLightning = true;
        } else {
            this.isLightning = false;
        }
    }

    redirectToLeadObject(event) {
        this.isModalOpen = true;
    }
    redirectToCampainManager() {
        var urlprefix;
        if (this.isLightning) {
            urlprefix = '/lightning/n/SML__Campaign_Manager';
        }
        else {
            urlprefix = '/apex/SML__Campaign_Manager'
        }

        this.redirection(urlprefix);
    }
    redirectToConverseDesk() {
        var urlprefix;
        if (this.isLightning) {
            urlprefix = '/lightning/n/smagicinteract__Lightning_Desk';
        }
        else {
            urlprefix = '/apex/smagicinteract__Lightning_Desk'
        }

        this.redirection(urlprefix);
    }
    redirectToLeadRecord() {
        getCreatedLeadId().then((response) => {
            var urlprefix = '/' + response;
            this.redirection(urlprefix);
        })
    }
    triggerInquiryFlow() {
        triggerInquiryMessage().then(
            (res) => {
                let toolTipdata = {
                    headerMessage: 'SUCCESS! INQUIRY QUALIFICATION FLOW HAS BEEN TRIGGERED.',
                    infoMessage: 'Pick your mobile phone and respond to the qualification messages.',
                    firstButtonText: 'Next',
                    firstActionFunction: function () {
                        console.log(this.onInquiryToolTipNext);
                        this.onInquiryToolTipNext();
                    }.bind(this)
                }
                this.openToolTip(toolTipdata);
            }
        ).catch((error) => {
            this.openToast('error', 'something went wrong', error, 'open');
        });
    }
    triggerNPSSurveyMessage() {
        triggerNPSSurveyMessage().then(
        );
    }
    redirection(urlprefix, openInsameWidnow) {
        if (!urlprefix) {
            return;
        }
        var sanitizedUrl;
        var el;
        if (!urlprefix || !urlprefix.indexOf('script:') !== -1 && urlprefix.indexOf('data:') !== -1) {
            return;
        }
        el = document.createElement('a');
        el.href = urlprefix;
        if (el.protocol === 'https:') {
            sanitizedUrl = encodeURI(urlprefix);
        }

        var url = sanitizedUrl;
        if (typeof sforce != 'undefined' && sforce.one) {
            sforce.one.navigateToURL(url);
        } else {
            if (openInsameWidnow) {
                window.open(sanitizedUrl);
            }
            else {
                window.open(sanitizedUrl, '_blank'); //window.location.href = url;
            }
        }
    }

    onModalCancel() {
        this.isModalOpen = false;
    }

    navigateToLeadRecordPage(event) {
        getObjectPrefix({
            objectName: event.target.name
        }).then((response) => {
            this.redirection('/' + response + '/o');
        }).catch((error) => {

        })
    }

    openToast(toastObject) {
        let toastRef = this.template.querySelector('c-toast-component');
        toastRef.setToastStyle(toastObject);
        //toastRef.setToastStyle('success', 'test', 'open');
    }
    openToolTip(toolTipObject) {
        let toastRef = this.template.querySelector('c-tooltip-component');
        console.log(toastRef)
        return toastRef.setToolTipStyle(toolTipObject);
    }
    onLeadCreationSuccess() {
        let toolTipdata = {
            headerMessage: 'Respond to double opt-in SMS.',
            infoMessage: 'Pick your mobile phone and respond to double opt-in SMS.',
            firstButtonText: 'Got It',
        }
        this.openToolTip(toolTipdata);
        this.onModalCancel();
    }

    onInquiryToolTipNext() {

        let toolTipdata = {
            headerMessage: 'VIEW INQUIRY QUALIFICATION THREAD',
            infoMessage: 'Click "Go To Converse Desk" to go to the Converse Inbox and view the messages exchanged.',
            firstButtonText: 'Go To Converse Desk',
            firstActionFunction: function () {
                //fire an event and show the 
                console.log(this.redirectToConverseDesk);
                this.redirectToConverseDesk();
            }.bind(this)
        }
        this.openToolTip(toolTipdata);
        console.log(this.openToolTip);
    }

    onSurveyToolTipNext() {
        let toolTipdata = {
            headerMessage: 'RESPOND TO SUPPORT AND SURVEY MESSAGES',
            infoMessage: 'Respond to support and survey messages received on your mobile phone.',
            firstButtonText: 'Got It',
            secondButtonText: 'Finish'
        }
        this.openToolTip(toolTipdata);
    }

    onSurveyBtnClick() {
        let toolTipdata = {
            headerMessage: 'EXPERIENCE SUPPORT AND SURVEY FLOW',
            infoMessage: 'To seek info on SMS-Magic pricing, pick your mobile phone and send "PRICING" in the same message window where you opted in to receive trial messages from SMS-Magic',
            //firstButtonText : 'Got It',
            firstButtonText: 'Next',
            firstActionFunction: function () {
                //fire an event and show the 
                console.log(this.redirectToConverseDesk);
                this.onSurveyToolTipNext();
            }.bind(this),
            //secondButtonText :'Next'
        }
        this.openToolTip(toolTipdata);
    }

    onViewButtonClicked() {
        if (this.isViewMoreClicked) {
            this.viewButtonText = 'View More'
            this.isViewMoreClicked = false
        }
        else {
            this.viewButtonText = 'View Less'
            this.isViewMoreClicked = true
        }
    }

    openModal(event) {
        let targetSet = event.target.dataset;

        let label = targetSet.case;
        let caseName = targetSet.caseName;
        let description = targetSet.description
        let isActive = targetSet.isactive === 'true' ? true : false;

        let image = this.caseImageMap[label];

        this.selectedCase = {
            title : label,
            category : caseName,
            description : description,
            image : image ? image : this.Grow_digram,
            isActive : isActive
        }
       
        this.isModalOpen = true;
    }

}
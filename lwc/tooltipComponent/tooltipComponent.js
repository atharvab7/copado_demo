import { LightningElement,api } from 'lwc';

export default class TooltipComponent extends LightningElement {

    toolTipData;
    showToolTip

    @api
    setToolTipStyle(toolTipData){
        this.toolTipData = {}
        this.showToolTip = false;

        setTimeout(()=>{
            this.toolTipData = toolTipData;
            this.showToolTip = true;
        },100)
       
        return true;
    }

    executeFirstButtonAction(){
        if(this.toolTipData.firstActionFunction && typeof this.toolTipData.firstActionFunction == 'function'){
            this.toolTipData.firstActionFunction();
        }
        this.closeToolTip()
    }

    executeSecondButtonAction(){
        if(this.toolTipData.secondActionFunction && typeof this.toolTipData.secondActionFunction == 'function'){
            this.toolTipData.secondActionFunction();
        }
        this.closeToolTip()
    }

    closeToolTip(){
        this.showToolTip = false
    }

    
}